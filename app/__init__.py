# RBSS WEB-IF init

import os
from flask import Flask
from flask_login import LoginManager
from app.config_rbss import basedir
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_pyfile('config_rbss.py')
db = SQLAlchemy(app)

lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

from app import views, models
