from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
nodes_services = Table('nodes_services', post_meta,
    Column('nodeId', Integer),
    Column('serviceId', Integer),
)

service = Table('service', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('uuid', String),
    Column('description', String),
)

RBSS = Table('RBSS', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('userId', Integer),
    Column('description', String),
)

node = Table('node', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('uuid', String),
    Column('nodeName', String(length=64)),
    Column('description', String),
    Column('rbssId', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['nodes_services'].create()
    post_meta.tables['service'].create()
    post_meta.tables['RBSS'].columns['description'].create()
    post_meta.tables['node'].columns['description'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['nodes_services'].drop()
    post_meta.tables['service'].drop()
    post_meta.tables['RBSS'].columns['description'].drop()
    post_meta.tables['node'].columns['description'].drop()
