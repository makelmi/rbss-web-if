from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
RBSS = Table('RBSS', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('userId', Integer),
)

node = Table('node', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('uuid', String),
    Column('nodeName', String(length=64)),
    Column('rbssId', Integer),
)

users_systems = Table('users_systems', post_meta,
    Column('userId', Integer),
    Column('systemId', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['RBSS'].create()
    post_meta.tables['node'].create()
    post_meta.tables['users_systems'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['RBSS'].drop()
    post_meta.tables['node'].drop()
    post_meta.tables['users_systems'].drop()
