from flask import render_template, flash, redirect, session, url_for, request, g
from flask_login import login_user, logout_user, current_user, login_required
from app import app, db, lm
from .forms import LoginForm
from .models import User
import pymongo, bcrypt, os

# Get status data from database
def getStatusDataFromDatabase():

    try:
        # Connect to database and choose collection
        conn = pymongo.MongoClient('mongodb://127.0.0.1:27017')
        db = conn['rbss']
        status = db['status']

        # Clean database for misreads
        status.delete_many({'temperature': 2147483642})
        status.delete_many({'humidity': 0.0})
        status.delete_many({'humidity': 2.6624670822171524e-44})
        status.delete_many({'temperature': 0.0})

        # Get all documents
        data = status.find({})
        return data

    except Exception as e:
        print(str(e))
        return

# Get alarm data from database
def getAlarmsDataFromDatabase():

    try:
        conn = pymongo.MongoClient('mongodb://127.0.0.1:27017')
        db = conn['rbss']
        alarms = db['alarms']
        data = alarms.find({})
        return data

    except Exception as e:
        print(str(e))
        return


# LoginManager user loader
@lm.user_loader
def load_user(userId):
    return User.query.get(int(userId))

# Before request (currently sets only global g.user)
@app.before_request
def before_request():
    g.user = current_user

# Main index
@app.route('/')
@app.route('/index')
@login_required
def main_page():
    return render_template("index.html", title='Home')


# Status view
@app.route('/status')
@login_required
def rbss():

    statusData = getStatusDataFromDatabase()

    return render_template("data.html",
                           title='Data',
                           statusData=statusData)

# Alarm view
@app.route('/alarms')
@login_required
def alarms():

    alarmData = getAlarmsDataFromDatabase()

    return render_template('alarms.html',
                            title='Alarms',
                            alarmData=alarmData)

@app.route('/delete/<filename>')
@login_required
def delete_row(filename):
    os.remove('/var/www/html/recordings/' + filename)
    conn = pymongo.MongoClient('mongodb://127.0.0.1:27017')
    db = conn['rbss']
    alarms = db['alarms']
    alarms.delete_one({'recording': filename})
    alarmData = getAlarmsDataFromDatabase()
    return render_template('alarms.html',
                            title='Alarms',
                            alarmData=alarmData)

# Login view
@app.route('/login', methods=['GET', 'POST'])
def login():

    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('rbss'))

    # Create the login form
    form = LoginForm()

    # Handle form data
    if form.validate_on_submit():

        # Query database for the entered username
        user = User.query.filter_by(userName=form.userName.data).first()
        session['rememberMe'] = form.rememberMe.data

        # If the user exists
        if user:

            # Compare password hashes
            if user.password == bcrypt.hashpw(form.password.data.encode('utf-8'),
                                        user.password):

                # Set user.authenticated to true and commit to db
                user.authenticated = True
                db.session.add(user)
                db.session.commit()
                login_user(user, remember=form.rememberMe.data)

                # Return a redirect to index
                return redirect('/')

        # If the entered user is not found, flash entered data (for debugging)
        else:
            flash('Login data: username "%s", password: "%s", rememberMe: "%s"' %
            (form.userName.data, form.password.data, str(form.rememberMe.data)))

    # Return the login view
    return render_template('login.html', title='RBSS Login', form=form)


# Logout view
@app.route('/logout', methods=['GET'])
@login_required
def logout():
    user = current_user
    username = user.userName
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return render_template('logout.html', username=username)
