from app import db


# Association table linking users to systems and vice versa
users_systems = db.Table('users_systems',
                db.Column('userId', db.Integer, db.ForeignKey('user.id')),
                db.Column('systemId', db.Integer, db.ForeignKey('rbss.id')))

# Association table linking nodes to services and vice versa
nodes_services = db.Table('nodes_services',
                db.Column('nodeId', db.Integer, db.ForeignKey('node.id')),
                db.Column('serviceId', db.Integer, db.ForeignKey('service.id')))


# Database model for users
class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String)
    email = db.Column(db.String(120), index=True, unique=True)
    authenticated = db.Column(db.Boolean, default=False)

    systems = db.relationship('RBSS', secondary=users_systems,
                                backref=db.backref('systems', lazy='dynamic'),
                                lazy='dynamic')

    @property
    def is_active(self):
        return True

    @property
    def is_authenticated(self):
        return self.authenticated

    @property
    def is_anonymous(self):
        return False


    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return '<User %r>' % (self.userName)


# Database model for RBSS System (local system = Rpi + genuino nodes)
class RBSS(db.Model):
    __tablename__ = 'rbss'
    id = db.Column(db.Integer, primary_key=True)
    userId = db.Column(db.Integer, db.ForeignKey('user.id'))
    description = db.Column(db.String)
    nodes = db.relationship('Node', backref='system', lazy='dynamic')

    def get_id(self):
        return self.id


# Database model for a single node
class Node(db.Model):
    __tablename__ = 'node'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String, index=True, unique=True)
    nodeName = db.Column(db.String(64))
    description = db.Column(db.String)
    rbssId = db.Column(db.Integer, db.ForeignKey('rbss.id'))

    services = db.relationship('Service', secondary=nodes_services,
                                backref=db.backref('services', lazy='dynamic'),
                                lazy='dynamic')

    def get_id(self):
        return self.id


# Database model for a service provided by a node
class Service(db.Model):
    __tablename__ = 'service'
    id = db.Column(db.Integer, primary_key = True)
    uuid = db.Column(db.String, index=True, unique=True)
    description = db.Column(db.String)

    def get_id(self):
        return self.id
