#!flask/bin/python
# Script for adding users to SQLite database

from getpass import getpass
import sys, bcrypt

from flask import current_app
from app import app, models, db

def print_db_menu():
    print('(1) Add a new user')
    print('(2) Print all users')
    print('(q) Exit')

def createUser():

    name = ''

    while name is '':

        name = input('Username: ')
        if models.User.query.filter_by(userName=name).first():
            print('Username already exists.')
            name = ''

    password = getpass()
    assert password == getpass('Password (again):')

    email = input('E-mail address: ')

    u = models.User(userName=name, password=bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()), email=email)

    db.session.add(u)
    db.session.commit()


def printUsers():
    users = models.User.query.all()

    for u in users:
        print(u.id, u.userName, u.password, u.email)


print_db_menu()

selection = input('[Menu] ')

while selection != 'q':
    if selection == '1':
        createUser()

    if selection == '2':
        printUsers()

    print_db_menu()
    selection = input('[Menu] ')
